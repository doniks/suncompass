/*
 * Copyright (C) 2019  Peter Putz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUNCOMPASSLIBQML_H
#define SUNCOMPASSLIBQML_H

#include <QObject>

class SunCompassLibQML: public QObject {
    Q_OBJECT

public:
    SunCompassLibQML(){}
    ~SunCompassLibQML() = default;

    Q_INVOKABLE double declination(int seconds);

    Q_INVOKABLE int standard_time_meridian(double longitude);

    Q_INVOKABLE double local_solar_time_hours(double longitude, int seconds, double timezone);

    Q_INVOKABLE double solar_hour_angle(double longitude, int seconds, double timezone);

    Q_INVOKABLE double elevation(double latitude, double longitude, int seconds, double timezone);

    Q_INVOKABLE double azimuth(double latitude, double longitude, int seconds, double timezone);



};

#endif
