/*
 * Copyright (C) 2019  Peter Putz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "SunCompassLibQML.h"

namespace lib{
#include "suncompasslib.h"
}


double SunCompassLibQML::declination(int seconds_since_epoc){
    return lib::declination(seconds_since_epoc);
}

int SunCompassLibQML::standard_time_meridian(double longitude){
    return lib::standard_time_meridian(longitude);
}

double SunCompassLibQML::local_solar_time_hours(double longitude, int seconds_since_epoc, double timezone){
    return lib::local_solar_time_hours(longitude, seconds_since_epoc, timezone);
}

double SunCompassLibQML::solar_hour_angle(double longitude, int seconds, double timezone){
    return lib::solar_hour_angle(longitude, seconds, timezone);
}

double SunCompassLibQML::elevation(double latitude, double longitude, int seconds, double timezone){
    return lib::elevation(latitude, longitude, seconds, timezone);
}

double SunCompassLibQML::azimuth(double latitude, double longitude, int seconds, double timezone){
    return lib::azimuth(latitude, longitude, seconds, timezone);
}

