import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

Rectangle{
    id: maind
    border.width: 2
    radius: units.gu(1)
    property int margin : units.gu(1.5)
    property int bMargin : units.gu(2.5)
    property int minW: content.minW + 2 * margin
    property int minH: content.minH + margin + bMargin
    property int fillThreshold: 100

    property bool fillParentW: parent.width < minW + fillThreshold
    property bool fillParentH: parent.height < minH + fillThreshold
    width:  fillParentW ? parent.width  - 2 * margin : minW
    height: fillParentH ? parent.height - margin - bMargin : minH

    color: "#c02d60a3"
    border.color: "#ee2d60a3"


    function log(){
        console.log("maind: content:", content.childrenRect, content.minW, maind.fillParentW, "maind.wh:", maind.width, maind.height, "p.wh:", parent.width, parent.height, "g.mwh:", content.minW, content.minH )
    }

    onWidthChanged: {
        log()
    }

//    Rectangle {
//        id: contentx
//        visible: false
//        color: "yellow"
//        property int minW: 400
//        property int minH: 400
//    }

    Item{
        id: content
        anchors.fill:parent
        anchors.margins: units.gu(2.5)
        property int minW: dtc.minW
        property int minH: dtc.minH
        DateTimeConfig{
            id: dtc
            anchors.centerIn: parent
            property int minW: childrenRect.width
            property int minH: childrenRect.height
        }
    }

//    MouseArea{
//        anchors.fill:parent
//        onClicked: {
//            log()
//        }

//    }

}
