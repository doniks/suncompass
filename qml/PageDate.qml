import QtQuick 2.7
import Ubuntu.Components 1.3

Page {
    id: page
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr("Set date and time")
    }

    Item {
        id: contents
        anchors {
            top:header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        DateTimeConfig{
            anchors.centerIn: parent
        }
    }
}
