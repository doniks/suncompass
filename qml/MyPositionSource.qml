import QtQuick 2.7
import QtPositioning 5.6

PositionSource {
    id: positionSource
    onPositionChanged: {
        //planet.source = "images/sun.png";
        var currentPosition = positionSource.position.coordinate
        console.log("onPositionChanged", currentPosition, map.center)
        map.center = currentPosition
    }

    onSourceErrorChanged: {
        console.log("onSourceErrorChanged")
        if (sourceError == PositionSource.NoError)
            return

        console.log("Source error: " + sourceError)
        //activityText.fadeOut = true
        stop()
    }

    onUpdateTimeout: {
        console.log("onUpdateTimeout")
        //activityText.fadeOut = true
    }
}
