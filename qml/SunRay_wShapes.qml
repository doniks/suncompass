import QtQuick 2.0
import QtQuick.Shapes 1.4


/*
        e  <- end of the ray
       / \
      /   \
     /     \   <------------------- ray
    /       \
   /         \
  /           \
 /             \
 |      x      | <- center of the sun = start of the ray
 \             |
  \           /   <---------------- sun
    \_______/
*/



    /*
      composed of two triangles forming the ray plus
      a circle forming the sun

            |  |
           /|  |\
          / |  | \
         /  |  |  \
        /   |  |   \
       /    |  |    \
      /_____|  |_____\

           _______
         /         \
        /           \
       |             |
       \             |
        \           /
          \_______/

      the ray is transparent at the outer edge and
      solid color at the start of the ray
      the sun is solid color in the center, transparent at the circumference
*/


Shape {
    id: sunRay
    width: 100
    height: parent.height / 2
    property double k: 0.2 // the steepness of the outer edge of the ray
    property int endX: parent.width/2
    property int endY: parent.height/2
    property color colorInner: "yellow"
    property color colorOuter: "transparent"
    property color colorRayInner: "yellow" // colorInner
    property color colorRayOuter: "transparent" //colorOuter
    //property color
    x: endX - width/2
    y: endY
    //x: main.centerX - width/2
    //y: 0 // main.centerY

    ShapePath {
        id: rayRight
        //strokeWidth: 1
        //strokeColor: "orange"
        strokeWidth: 0
        strokeColor: "transparent"
        // x goes left to right
        // y goes top to bottom
        property int x0: sunRay.width/2
        property int y0: 0
        property int x1: rayRight.x0 + sunRay.height * sunRay.k
        property int y1: sunRay.height
/*  |  [end of the ray] (x0,y0)
|\
| \
|  \
|   \
|beta\
|     \
|      \
|       \
|        \ c
a |         \
|          \
|           \
|            \
|      h   __/\
|      __/     \
|  __/          \
|/_______________\ alpha (x1,y1)
gamma=90      b
[start of the ray]
sunsphere goes here                                */
        property double a: sunRay.height
        property double b: rayRight.x1 - rayRight.x0
        // tan(beta) * a = b
        property double beta: Math.atan( rayRight.b / rayRight.a )
        property double gamma: 90.0 - rayRight.beta
        property int hx0: rayRight.x0
        property int hy0: rayRight.y1
        // h = sin(beta) * a
        property double h: Math.sin(beta) * rayRight.a
        property double hdx: rayRight.h * Math.cos(beta)
        property double hdy: rayRight.h * Math.sin(beta)
        property int hx1: rayRight.x0 + rayRight.hdx
        property int hy1: rayRight.y1 - rayRight.hdy
        startX:       rayRight.x0 ; startY: rayRight.y0 ;
        PathLine { x: rayRight.x1 ; y:      rayRight.y1 }
        PathLine { x: rayRight.x0 ; y:      rayRight.y1 }
        PathLine { x: rayRight.x0 ; y:      rayRight.y0 }
        fillGradient: LinearGradient {
            //id: gradRight
            // from the start of the ray towards the edge
            x1: rayRight.hx0; y1: rayRight.hy0
            x2: rayRight.hx1; y2: rayRight.hy1
            GradientStop { position: 0.0; color: colorRayInner } // "#ddefef34" }
            //GradientStop { position: 0.4; color: "#66efef34" }
            GradientStop { position: 1.0; color: colorRayOuter } // "#00efef34" }
        }
    }
    ShapePath {
        id: rayLeft
        property int x0: rayRight.x0
        property int y0: rayRight.y0
        property int x1: rayLeft.x0 - sunRay.height * sunRay.k
        property int y1: sunRay.height
        property int hx0: rayLeft.x0
        property int hy0: rayLeft.y1
        property int hx1: rayLeft.x0 - rayRight.hdx
        property int hy1: rayLeft.y1 - rayRight.hdy
        strokeWidth: 0
        strokeColor: "transparent"
        startX:       rayLeft.x0 ; startY: rayLeft.y0 ;
        PathLine { x: rayLeft.x1 ; y:      rayLeft.y1 }
        PathLine { x: rayLeft.x0 ; y:      rayLeft.y1 }
        PathLine { x: rayLeft.x0 ; y:      rayLeft.y0 }
        fillGradient: LinearGradient {
            //id: gradLeft
            // from the start of the ray towards the edge
            x1: rayLeft.hx0; y1: rayLeft.hy0
            x2: rayLeft.hx1; y2: rayLeft.hy1
            GradientStop { position: 0.0; color: colorRayInner } // "#ddefef34" }
            // GradientStop { position: 0.4; color: "#66efef34" }
            GradientStop { position: 1.0; color: colorRayOuter } // "#00efef34" }
        }
    }
    ShapePath {
        id: sun
        property int diameter: rayRight.b * 2
        property int radius: sun.diameter/2
        // path forms a square
        // the gradient will make it look like a circle
        startX: rayLeft.x1; startY: rayLeft.y1 - sun.radius
        PathLine { relativeX:  sun.diameter; relativeY: 0 }
        PathLine { relativeX:  0;            relativeY: sun.diameter }
        PathLine { relativeX: -sun.diameter; relativeY: 0 }
        PathLine { relativeX:  0;            relativeY: -sun.diameter }
        //strokeWidth: 2
        //strokeColor: "orange"
        strokeWidth: 0
        strokeColor: "transparent"
        fillGradient: RadialGradient {
            centerX: sun.startX + sun.radius
            centerY: sun.startY + sun.radius
            centerRadius: sun.radius
            focalX: centerX; focalY: centerY
            GradientStop { position: 0.0; color: colorInner } //  "yellow" }
            GradientStop { position: 0.4; color: colorInner } //  "yellow" }
            GradientStop { position: 1.0; color: colorOuter } //  "transparent" }
        }
    }
    Component.onCompleted: {
//        colorRayInner = colorRayInner
//        colorRayOuter = colorRayOuter
//        console.log("rayRight ", rayRight.hx0, rayRight.hy0, rayRight.hx1, rayRight.hy1)
//        console.log("rayLeft ", rayLeft.hx0, rayLeft.hy0, rayLeft.hx1, rayLeft.hy1)
    }
}
