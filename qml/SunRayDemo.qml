import QtQuick 2.0
import QtQuick.Shapes 1.4
import QtQml 2.0
import "."



Rectangle{
    id:main
    width: 500
    height: 600
    color: "lightblue"

    // calculate "center", ie the center of the screen
    property double centerY: height/2
    property double centerX: width/2

    property int dawn: 40
    property int predawn: 10
    property var yellowCA: makeColorArray( 0xff, 0xff, 0x3c, 0xff )
    property color yellow: makeColor(yellowCA)
    property var transparentCA: makeColorArray( 0x00, 0x00, 0x00, 0x00 )
    property var orangeCA: makeColorArray( 0xff, 0x9e, 0x1b, 0xff )
    property var greyCA: makeColorArray( 0xb2, 0xb2, 0xa7, 0xff )
    property color grey: makeColor(greyCA)


    function deg2rad(deg){
        return deg / 180 * Math.PI
    }

    function rad2deg(rad){
        return rad * 180 / Math.PI
    }

    function makeColorInterpolate(colorA, colorB, fraction)
    {
        var newColor = []
        //console.log("mcI", colorA, colorA.length, colorB, fraction, newColor)
        for ( var i = 0 ; i < colorA.length ; ++i ) {
            //console.log(i)
            //console.log(colorA[i])
            newColor[i] = colorA[i] * fraction + colorB[i] * (1.0 - fraction)
            //console.log(i, newColor[i])
        }
        //console.log("mcI", newColor)
        return newColor
    }

    function makeColor(colorArray) {
        if ( colorArray.length === 3 )
            return Qt.rgba(colorArray[0], colorArray[1], colorArray[2], 1)
        else
            return Qt.rgba(colorArray[0], colorArray[1], colorArray[2], colorArray[3])
    }

    function makeColorArray(red, green, blue, alpha){
        var array = [0,0,0,0]
        array[0] = red / 0xff
        array[1] = green / 0xff
        array[2] = blue / 0xff
        array[3] = alpha / 0xff
        return array
     }

    function sunColor(angle) {
        var offset, fraction
        if ( angle > 270 + predawn ) {
            return main.grey
        } else if ( angle > 270 ) {
            offset = angle - 270
            fraction = offset / predawn
            return makeColor( makeColorInterpolate(main.greyCA, main.orangeCA, fraction ) )
        } else if ( angle > 270 - dawn ) {
            offset = 270 - angle
            fraction = offset / dawn
            return makeColor( makeColorInterpolate(main.yellowCA, main.orangeCA, fraction ) )
        } else if ( angle > 90 + dawn ) {
            return main.yellow
        } else if ( angle > 90 ) {
            offset = angle - 90
            fraction = offset / dawn
            return makeColor( makeColorInterpolate(main.yellowCA, main.orangeCA, fraction ) )
        } else if ( angle > 90 - predawn ) {
            offset = 90 - angle
            fraction = offset / predawn
            return makeColor( makeColorInterpolate(main.greyCA, main.orangeCA, fraction ) )
        } else {
            return main.grey
        }

    }


    function rayColor(angle) {
        var offset, fraction
        if ( angle > 270 + predawn ) {
            return "transparent"
        } else if ( angle > 270 ) {
            offset = angle - 270
            fraction = offset / predawn
            return makeColor( makeColorInterpolate(main.transparentCA, main.orangeCA, fraction ) )
        } else if ( angle > 270 - dawn ) {
            offset = 270 - angle
            fraction = offset / dawn
            return makeColor( makeColorInterpolate(main.yellowCA, main.orangeCA, fraction ) )
        } else if ( angle > 90 + dawn ) {
            return main.yellow
        } else if ( angle > 90 ) {
            offset = angle - 90
            fraction = offset / dawn
            return makeColor( makeColorInterpolate(main.yellowCA, main.orangeCA, fraction ) )
        } else if ( angle > 90 - predawn ) {
            offset = 90 - angle
            fraction = offset / predawn
            return makeColor( makeColorInterpolate(main.transparentCA, main.orangeCA, fraction ) )
        } else {
            return "transparent"
        }

    }


    SunRay {
        id: sunRay
        width: 100

        //height: Math.min( parent.height / 2, parent.width / 2 )
        height: Math.min(main.centerY / Math.abs(Math.cos(deg2rad(rot.angle))),
                         main.centerX / Math.abs(Math.sin(deg2rad(rot.angle))) )

        //colorInner: "yellow"
        colorInner: sunColor(rot.angle)
        colorRayInner: rayColor(rot.angle)
        //colorInner: Qt.binding(function() { return inputHandler.pressed ? "steelblue" : "lightsteelblue" });
        //property int stuff: 0
        property double k: 0.2 // the steepness of the outer edge of the ray
        endX: main.centerX //- sunRay.width/2
        endY: main.centerY //+ 20

        transform: Rotation{
            id: rot
            origin.x: sunRay.width / 2 // sunRay.endX
            origin.y: 0 // sunRay.endY
            // angle is 0 at original position and then clockwise
        }

        function turn(){
            rot.angle = (rot.angle + 1) % 360
        }

        Timer{
            id: rotationTimer
            interval:200
            running:true
            repeat:running
            onTriggered: {
                sunRay.turn()
            }
        }


        // allow to manually "dial" the sun ray
        MouseArea{
            anchors.fill:parent
            cursorShape:Qt.OpenHandCursor
            preventStealing: true
            onPositionChanged: {
                rotationTimer.running = false
                var point = mapToItem(main, mouse.x, mouse.y)
                var relativeX = point.x - main.centerX
                var relativeY = point.y - main.centerY
                //console.log(mouseX, mouseY, relativeX, relativeY)
                var rad = Math.atan( Math.abs( relativeY / relativeX ) )
                var deg = rad2deg(rad)
                console.log(" mouse= ", mouse.x.toFixed(0), mouse.y.toFixed(0),
                            " point= ", point.x.toFixed(0), point.y.toFixed(0),
                            " rel= ", relativeX.toFixed(0), relativeY.toFixed(0))


                if ( relativeX >= 0 && relativeY < 0 ) {
                    rot.angle = 270 - deg
                    console.log("top right", deg.toFixed(0), rot.angle.toFixed(0))
                } else if ( relativeX >= 0 && relativeY >= 0 ){
                    rot.angle = 270 + deg
                    console.log("bottom right", deg.toFixed(0), rot.angle.toFixed(0))
                } else if ( relativeX < 0 && relativeY >= 0 ){
                    rot.angle = 90 - deg
                    console.log("bottom left", deg.toFixed(0), rot.angle.toFixed(0))
                } else if ( relativeX < 0 && relativeY < 0 ){
                    rot.angle = 90 + deg
                    console.log("top left", deg.toFixed(0), rot.angle.toFixed(0))
                }
            }
        }

    }


    Component.onCompleted: {
        var a = [0.9, 0.9, 0.4] // yellow "#efef34"
        var b = [1.0, 1.0, 1.0] // "#ffffff"
        var c = makeColorInterpolate(a, b, 0.9)
        console.log(a, b, c, makeColor(c))
    }
}
