import QtQuick 2.7
import SunCompassLibQML 1.0

Item {
    id: state
    property string peter: "Jo'sup"

    property var date: new Date()
//    property alias latitude: map.center.latitude
//    property alias longitude: map.center.longitude
    property real latitude: 0
    property real longitude: 0
    property int  locationTZ: SunCompassLibQML.standard_time_meridian(scstate.longitude)

    property int  seconds: date/1000
    property real dateTZ: -1 * date.getTimezoneOffset()/ 60.0

    property real lst_hours: SunCompassLibQML.local_solar_time_hours(longitude, seconds, dateTZ)
    property real declination: SunCompassLibQML.declination(seconds)
    property real solar_hour_angle: SunCompassLibQML.solar_hour_angle(longitude, seconds, dateTZ)
    property real elevation: SunCompassLibQML.elevation(latitude, longitude, seconds, dateTZ)
    property real azimuth: SunCompassLibQML.azimuth(latitude, longitude, seconds, dateTZ)
}
