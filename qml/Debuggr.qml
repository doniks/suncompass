import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

Rectangle{
    id: maind
    //visible: true
    enabled: visible
    color: "#cc2d60a3" // form: #AARRGGBB
    border.color: "#dd2d60a3"
    border.width: 2
    radius: units.gu(1)
    property int margin : units.gu(1.5)
    property int bMargin : units.gu(2.5)
    property int minW: content.minW + 2 * margin
    property int minH: content.minH + margin + bMargin
    property int fillThreshold: 100

    property bool fillParentW: parent.width < minW + fillThreshold
    property bool fillParentH: parent.height < minH + fillThreshold
    width:  fillParentW ? parent.width - 2 * margin        : content.minW
    height: fillParentH ? parent.height - margin - bMargin : content.minH


    function log(){
//        console.log("maind: g.cR:", content.childrenRect, maind.fillParent, "maind.wh:", maind.width, maind.height, "p.wh:", parent.width, parent.height, "g.mwh:", content.minW, content.minH )
    }

    onWidthChanged: {
        log()
    }

    Grid{
        id: content
        anchors.fill:parent
        property int margin: units.gu(2)
        anchors.margins: margin
        columns: 2
        spacing: units.gu(1)
        property int minW: childrenRect.width + 2 * margin
        property int minH: childrenRect.height + 2 * margin
        DebugLabel{ text: "latitude" }
        DebugLabel{ text: scstate.latitude.toFixed(3) }

        DebugLabel{ text: "longitude" }
        DebugLabel{ text: scstate.longitude.toFixed(3) }

        DebugLabel{ text:"Location TZ" }
        DebugLabel{ text: scstate.locationTZ }

        DebugLabel{ text:"UTC" }
        DebugLabel{ text: scstate.date.toUTCString() }

        DebugLabel{ text:"local" }
        DebugLabel{
            //text: scstate.date.toLocaleString("en-US",{timezone:"America/New_York",hour12:false,weekday:"short",month:"short",day:"2-digit",hour:"2-digit",minute:"2-digit",second:"2-digit",year:"numeric",timeZoneName:"short"})
            // none of this works, I think we might have to print this from C++ using Howard Hinnant timezone DB
            text: "FIXME"
        }

        DebugLabel{ text:"System TZ" }
        DebugLabel{ text: scstate.dateTZ; }

        DebugLabel{ text:"lst (incl eot)" }
        DebugLabel{ text: scstate.lst_hours.toFixed(2)  }

        DebugLabel{ text: "declination" }
        DebugLabel{ text: scstate.declination.toFixed(2) }

        DebugLabel{ text: "solar hour angle" }
        DebugLabel{ text: scstate.solar_hour_angle.toFixed(2) }

        DebugLabel{ text: "elevation" }
        DebugLabel{ text: scstate.elevation.toFixed(2) }

        DebugLabel{ text: "azimuth" }
        DebugLabel{ text: scstate.azimuth.toFixed(2) }
    }

//    MouseArea{
//        anchors.fill:parent
//        onClicked: {
//            log()
//        }

//    }

}
