import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Pickers 1.3

Item{
    id: dtc
    anchors.centerIn:parent
    Column {
        id: contentsColumn
        anchors.centerIn: parent
        spacing: units.gu(4)

        Item {
            width: Math.max( grid.width, datePickerRow.width)
            height: grid.height
            Grid {
                id: grid
                columns: 2
                anchors.centerIn: parent
                spacing: units.gu(2)
                Label {
                    text: "Date:"
                }
                Label {
                    id: dateLabel
                    width: units.gu(20)
                    horizontalAlignment: Text.AlignRight
                    text: Qt.formatDate(scstate.date, "ddd, dd-MMM-yyyy")
                }
                Label {
                    text: "Time:"
                }
                Label {
                    id: timeLabel
                    width: dateLabel.width
                    horizontalAlignment: Text.AlignRight
                    text: Qt.formatTime(scstate.date, "hh:mm:ss")
                }
            }
        }

        Row{
            id: switchRow
            spacing: units.gu(2)
            Switch{
                id: freezeSwitch
                onCheckedChanged: {
                    root.clockFreeze = checked
                }
            }
            Binding {
                target: freezeSwitch
                property: "checked"
                value: root.clockFreeze
            }
            Label {
                text: i18n.tr("Freeze date and time")
            }
        }

        Row {
            id: rateSliderRow
            Column{
                Row{
                    Label{
                        id: rateLabel
                        text: i18n.tr("Rate:")
                    }
                }
                Slider {
                    id: rateSlider
                    // precalculate the number of seconds in natural units for display and conversion
                    property real minute: 60
                    property real hour: minute * 60
                    property real day: hour * 24
                    property real week: day * 7
                    property real month: day * 30
                    property real year: day * 365
                    minimumValue: 0
                    maximumValue: Math.log(week)
                    width: contentsColumn.width
                    stepSize: 0.1
                    live: true
function writeLabel() {
    var seconds = Math.round(Math.exp(Math.round(value*10)/10))
    var prefix = "Rate: "
    var r = 0.0;
    if ( seconds < 2 ){
        rateLabel.text = prefix + "normal"
        return 1
    } else if ( seconds < 10 ) {
        // until 10s: {2,3,4,5,6,7,8,9} s/s
        r = seconds
        rateLabel.text = prefix + r
                + " seconds per second"
        return r
    } else if ( seconds < minute ) {
        // until 1m: {10, 20, 30, 40, 50} s/s
        r = Math.round(seconds / 10 ) * 10
        rateLabel.text = prefix + r
                + " seconds per second"
        return r
    } else if ( seconds < 10 * minute ) {
        // until 10m: {1,2,3,4,5,6,7,8,9} m/s
        r = Math.round(seconds / minute)
        rateLabel.text = prefix + r
                + " minutes per second"
        return r * minute
    } else if ( seconds < hour ) {
        // until 1h: {10,20,30,40,50} m/s
        r = Math.round(seconds / minute / 10) * 10
        rateLabel.text = prefix + r
                + " minutes per second"
        return r * minute
    } else if ( seconds < 3 * hour ) {
        // until 3h: {1,2,3} h/s
        r = Math.round(seconds / hour)
        rateLabel.text = prefix + r
                + " hours per second"
        return r * hour
    } else if ( seconds < 12 * hour ) {
        // until 12h: {3,6,9,12} h/s
        r = Math.round(seconds / hour / 3 ) * 3
        rateLabel.text = prefix + r
                + " hours per second"
        return r * hour
    } else if ( seconds < day ) {
        // until 1d: {12, 18, 24} hours per second
        r = Math.round(seconds / hour / 6 ) * 6
        rateLabel.text = prefix + r
                + " hours per second"
        return r * hour
    } else if ( seconds < week ) {
        // until 1w: days per second
        r = Math.round(seconds / day)
        rateLabel.text = prefix + r
                + " days per second"
        return r * day
    } else if ( seconds < month ) {
        // until 1month: weeks per second
        r = Math.round(seconds / week)
        rateLabel.text = prefix + r
                + " weeks per second"
        return r * week
    } else if ( seconds < year ) {
        // until 1year: months per second
        r = Math.round(seconds / month)
        rateLabel.text = prefix + r
                + " months per second"
        return r * month
    } else {
        // years per second
        r = Math.round(seconds / year)
        rateLabel.text = prefix + r
                + " years per second"
        return r * year
    }
}
                    function formatValue(v){
                        return "";
                    }

                    onValueChanged:{
                        var seconds = Math.exp(value)
                        var rate = writeLabel()
                        //root.clockRate = rate
                        //console.log("oVC:", value, pressed, root.clockRate, rate, seconds)
                    }
                    Component.onCompleted: {
                        value = Math.log(root.clockRate)
                        var rate = writeLabel()
                    }
                    onTouched: {
                        console.log("oT:", onThumb)
                    }
                    onPressedChanged: {
                        // var seconds = Math.exp(value)
                        var rate = writeLabel()
                        root.clockRate = rate
                        console.log("oPC:", value, pressed, root.clockRate, rate)
                    }
                }
            }
        }

        Row{
            id: datePickerRow
            spacing: units.gu(1)

            DatePicker {
                id: datePicker
                width: units.gu(22)
                mode: "Days|Months|Years"
                minimum: {
                    var d = new Date(scstate.date);
                    d.setFullYear(d.getFullYear() - 100);
                    return d;
                }
                maximum: {
                    var d = new Date(scstate.date);
                    d.setFullYear(d.getFullYear() + 100);
                    return d;
                }
            }
            DatePicker {
                id: timePicker
                width: units.gu(7)
                mode: "Hours|Minutes"
            }
            function set(d){
                datePicker.date = new Date(d)
                timePicker.date = new Date(d)
            }
            function apply(){
                scstate.date = new Date(
                    datePicker.date.getFullYear(),
                    datePicker.date.getMonth(),
                    datePicker.date.getDate(),

                    timePicker.date.getHours(),
                    timePicker.date.getMinutes() )
                scstate.dateTZ = timezonePicker.get()
            }

            Picker {
                id: timezonePicker
                width: units.gu(10)
                model: ["-12", "-11", "-10", "-9", "-8", "-7", "-6 CT", "-5", "-4", "-3", "-2", "-1", "0 UTC", "1", "2"]
                delegate: PickerDelegate {
                    Label {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 17 // 20 is slightly too much
                        text: modelData
                    }
                }
                onSelectedIndexChanged: {
                    print("selected month: " + selectedIndex);
                }
                function set( timezone ){
                    selectedIndex = 12 + timezone
                }
                function get(){
                    return selectedIndex -12
                }
            }
        }

        Grid{
            id: buttonRow
            spacing: units.gu(1)
            columns: 4
            Button {
                text: i18n.tr("Apply")
                onClicked:{
                    datePickerRow.apply()
                }
            }
            Button {
                text: i18n.tr("Now")
                onClicked:{
                    datePickerRow.set(new Date())
                }
            }

            Button {
                text: i18n.tr("Equinox")
                onClicked:{
                    datePickerRow.set( new Date(2019, 2, 20, 20, 58, 0, 0) ) // Mar-20 20:58 Equinox
                }
            }
            Button {
                text: i18n.tr("Example")
                onClicked:{
                    // example
                    // We’ll use as an example the
                    // site of the Gateway Arch in Saint Louis, Missouri,
                    // located at 38.6N, 90.2W,
                    // at 7:00am on February 7, 2012
                    // and find the Sun’s elevation and azimuth.
                    // Central Time Zone (Jefferson City, Missouri)
                    // CT = -6
                    // (CDT = -5, Daylight savings)
                    datePickerRow.set( new Date(2012, 2, 7, 7-6, 0, 0) ) // FIXME: I'm just subtracting the tz offset from the hours :-P
                    map.center.latitude = 38.625 // 38.6
                    map.center.longitude = -90.185 // -90.2
//                    scstate.latitude = 38.6
//                    scstate.longitude = - 90.2
                    timezonePicker.set( -6 )
                    datePickerRow.apply()

                    // read it back
                    datePickerRow.set(scstate.date)
                    timezonePicker.set(scstate.dateTZ)
                }
            }



        }

    }
    Component.onCompleted: {
        datePickerRow.set(scstate.date)
        timezonePicker.set(scstate.dateTZ)

    }

}
