import QtQuick 2.7
import QtLocation 5.6
import QtPositioning 5.6

Map {
    id: map
    anchors.fill: parent
    plugin: mapPlugin
    //center: QtPositioning.coordinate(settings.centerLat, settings.centerLon) // 59.91, 10.75) // Oslo
    zoomLevel: settings.zoomLevel // 1 // 14 city surroundings

    gesture.enabled: true
    // accept all gestures except
    // - rotate, to keep north up (MapGestureArea.RotationGesture)
    // - tilt, because I just don't understand what it does :-P  | MapGestureArea.TiltGesture
    gesture.acceptedGestures: MapGestureArea.PinchGesture  | MapGestureArea.FlickGesture | MapGestureArea.PanGesture

    Component.onCompleted: {
        center = QtPositioning.coordinate(settings.centerLat, settings.centerLon)
    }

    // bulls eye at the center of the screen
    Rectangle {
        id: bullseye
        width: units.gu(2)
        height: width
        anchors.centerIn:parent
        color:"transparent"
        border.color: "grey"// "#0a2f4a"
        border.width: 1 // units.gu(1/10)
        radius: width*0.5
        Rectangle {
            width: parent.width / 2
            height: width
            anchors.centerIn:parent
            color:"transparent"
            border.color: "grey"// "#0a2f4a"
            border.width: 1 // units.gu(1/10)
            radius: width*0.5
        }
    }

    Item {
        id: sun_with_ray
        z: parent.z + 2

        // width of the ray, diameter of the sun
        width: units.gu(22)
        // the "periphery" of the sun.
        // 0 means the center of the sunsphere is exactly on the edge of the screen
        // negative means it is further inward
        // positive means it is further outward
        // FIXME: probably better to define this as a percentage of the lenght of the ray
        property int periphery: units.gu(8)
        // the length of the ray
        // measured from the center of the sunsphere to the end of the ray
        // (center of the screen)
        height: Math.min(main.centerY / Math.abs(Math.cos(deg2rad(rot.angle))) + periphery,
                         main.centerX / Math.abs(Math.sin(deg2rad(rot.angle))) + periphery)

        // place the "end" of the ray (ie, the bottom, center of sun_with_ray)
        // in the center of the screen
        // NB: x,y specifies the position the top left corner of the
        // Rectangle measured from top left
        x: main.centerX - width/2
        y: main.centerY - height

        Rectangle{
            id: ray
            anchors.fill:parent
            gradient: Gradient {
                // position is from 1.0 (top) to 0.0 (bottom)
                GradientStop { position: 0.0; color: "#FBFFCC" }
                GradientStop { position: 0.9; color: "transparent" }
            }
        }

        Rectangle{
            id: sunsphere
            anchors.horizontalCenter:parent.horizontalCenter
            anchors.verticalCenter:parent.top
            width: parent.width
            height: width
            radius: width/2
            color: (scstate.elevation > 0)?"#FAFF00":"grey"
        }

        transform: Rotation{
            id: rot
            origin.x:sun_with_ray.width/2
            origin.y:sun_with_ray.height
            //angle: SunCompassLibQML.solar_hour_angle(longitude, seconds)
            //   0 top
            //  90 right
            // 180 bottom
            // 280 left
            // 360 top
            angle: scstate.azimuth
        }

        /*
        // allow to manually "dial" the sun ray
        MouseArea{
            anchors.fill:parent
            cursorShape:Qt.OpenHandCursor
            preventStealing: true
            onPositionChanged: {
                var point = mapToItem(main, mouse.x, mouse.y)
                var relativeX = point.x - main.centerX
                var relativeY = point.y - main.centerY
                //console.log(mouseX, mouseY, relativeX, relativeY)
                var rad = Math.atan( Math.abs( relativeY / relativeX ) )
                var deg = rad2deg(rad)

                if ( relativeX >= 0 && relativeY < 0 ) {
                    rot.angle = 90 - deg
                    console.log("top right", rot.angle)
                } else if ( relativeX >= 0 && relativeY >= 0 ){
                    rot.angle = 90 + deg
                    console.log("bottom right", rot.angle)
                } else if ( relativeX < 0 && relativeY >= 0 ){
                    rot.angle = 270 - deg
                    console.log("bottom left", rot.angle)
                } else if ( relativeX < 0 && relativeY < 0 ){
                    rot.angle = 270 + deg
                    console.log("top left", rot.angle)
                }
            }
        }*/

        /*
        function turn(){
            rot.angle = (rot.angle + 1) % 360
        }

        Timer{
            id: rotationTimer
            interval:200
            running:false
            repeat:running
            onTriggered: {
                sun_with_ray.turn()
            }
        }*/
    }
}
