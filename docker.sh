APPINST=/home/peter/docs/devel/suncompass/build/x86_64-linux-gnu/app/install

docker run \
--privileged \
-v /home/peter/docs/devel/suncompass:/home/peter/docs/devel/suncompass:Z \
-v /home/peter/.clickable/home:/home/phablet:Z \
\
-v /tmp/.X11-unix:/tmp/.X11-unix:Z -v /tmp/.docker.xauth:/tmp/.docker.xauth:Z -v /dev/shm:/dev/shm:Z -v /etc/machine-id:/etc/machine-id:Z -v /run/1000/pulse:/run/user/1000/pulse:Z -v /var/lib/dbus:/var/lib/dbus:Z -v /home/peter/.pulse:/home/phablet/.pulse:Z -v /dev/snd:/dev/snd:Z -e XAUTHORITY=/tmp/.docker.xauth \
-e DISPLAY=:0 \
-e QML2_IMPORT_PATH=$APPINST/lib/x86_64-linux-gnu:$APPINST/lib:/usr/local/nvidia/lib:/usr/local/nvidia/lib64 \
-e LD_LIBRARY_PATH=$APPINST/lib/x86_64-linux-gnu:$APPINST/lib:/usr/local/nvidia/lib:/usr/local/nvidia/lib64 \
-e PATH=/usr/local/nvidia/bin:/bin:/usr/bin:$APPINST/bin:$APPINST/lib/x86_64-linux-gnu/bin:$APPINST \
-e HOME=/home/phablet -e OXIDE_NO_SANDBOX=1 -e UBUNTU_APP_LAUNCH_ARCH=x86_64-linux-gnu \
-w $APPINST \
--user=1000 --rm -it clickable/ubuntu-sdk:16.04-amd64 \
bash


# -c 'qmlscene qml/Main.qml'


