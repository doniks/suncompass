[![pipeline status](https://gitlab.com/doniks/suncompass/badges/master/pipeline.svg)](https://gitlab.com/doniks/suncompass/commits/master)

<img width="200px" src="assets/iconsun2.png" />

# Sun Compass

Sun Compass is an app that helps you orient yourself based on the direction of the sun.

The app shows a map and a sun ray. Turn the phone such that the sun ray points
towards the sun. Now your phone is oriented north and the map is properly
aligned with your surroundings.

In order for the app to determine the correct direction of the sun it relies on
your local time and your GPS position.

## Development

The app is built for [Ubuntu Touch](https://ubuntu-touch.io/). It uses the [SunCompassLib](https://gitlab.com/doniks/suncompasslib) library for the calculation of the direction.
The library is embedded as a git submodule. To build the app you need [clickable](http://clickable.bhdouglass.com).

```
git clone --recursive git@gitlab.com:doniks/suncompasslib.git
cd suncompass
clickable
```

If at a later point you want to update your local copy from upstream, remember to also update the submodule with `git submodule update --init --recursive`.


## Feedback

Happy to hear your feedback, suggestions or issues you encounter. Find me on gitlab [issue tracker](https://gitlab.com/doniks/), as dohniks on telegram, or on the [UBports forum](https://forums.ubports.com/user/doniks).

